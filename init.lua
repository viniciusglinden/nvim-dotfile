require 'compatibility'
require 'config.lazy'

-- start debugger before launching the configuration:
-- nvim --cmd "lua launch_neovim_debug=true"
---@diagnostic disable-next-line: undefined-global
if launch_neovim_debug then
    require('osv').launch { port = 8086, blocking = true }
end

require 'config.keybind'
require 'config.settings'
require 'config.completion'

require 'plugins.bclose'

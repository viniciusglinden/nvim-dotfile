--[[
Not plugin-related keybindings
https://github.com/nanotee/nvim-lua-guide#definingutils.-mappings
--]]

local utils = require 'utils'

utils.nmap('New line', '<c-o>', 'o<esc>')
utils.nmap('Next buffer', '<c-l>', ':bn!<CR>')
utils.nmap('Previous buffer', '<c-h>', ':bp!<CR>')
utils.nmap('Upper line (always)', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true })
utils.nmap('Lower line (always)', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true })
--[[
TODO implement this
map('n', '<leader>t', ..., { silent = true, desc = 'Convert tabs to space and vice-versa' })
set expandtab<CR>:%retab!<CR>
set noexpandtab<CR>:%retab!<CR>
--]]
utils.nmap('Go up and centralize', '<c-u>', '10kzz')
utils.nmap('Go down and centralize', '<c-d>', '10jzz')
utils.nmap('Yank until the EOL', 'Y', 'y$')
utils.nmap('Unindent', '<', '<<')
utils.nmap('Indent', '>', '>>')
utils.nmap('Format line', '=', '==')
utils.nmap('Switch to last buffer', '<space>', ':b#<cr>')
utils.nmap('Reload nvim configuration', '<leader><cr>', function()
    utils.reload()
end)
utils.nmap('Open file with xdg-open', '<leader>o', function()
    vim.cmd [[ :! xdg-open '%:p'
        redraw ]]
end)
utils.imap('Go to the end of line', '<c-a>', '<esc>A')

-- spell check related
local function spellhelper(lang)
    -- TODO create a menu for this
    --https://andrewcourter.substack.com/p/writing-your-first-telescope-extension
    local char = string.sub(lang, 1, 1)
    local lower = string.lower(lang)
    local upper = string.upper(lang)

    utils.nmap('Set buffer language to ' .. upper, '<leader>l' .. char .. char, function()
        vim.bo.spelllang = lower
        print('Language: ' .. vim.bo.spelllang)
    end)
    utils.nmap('Append ' .. upper .. ' to buffer language', '<leader>l' .. char, function()
        -- TODO use vim.tbl_filter
        local lang_table = utils.split(vim.bo.spelllang, ',', true)
        lang_table[lower] = true
        lang_table = utils.table_reverse(lang_table, true)
        vim.bo.spelllang = table.concat(lang_table, ',')
        print('Language: ' .. vim.bo.spelllang)
    end)
end

utils.nmap('Toggle spell check', '<leader>l', ':set invspell<CR>')
spellhelper 'de_20'
spellhelper 'en_us'
spellhelper 'pt'

utils.nmap('Reload nvim configuration', '<leader><cr>', function()
    utils.reload()
end)
utils.nmap('Join lines without jumping', 'J', function()
    local current_pos = vim.fn.getpos '.'
    vim.cmd 'join'
    vim.fn.setpos('.', current_pos)
end)
utils.xmap('Move line up', 'K', ":move '<-2<CR>gv-gv")
utils.xmap('Move line down', 'J', ":move '>+1<CR>gv-gv")
-- NOTE there is currently a bug where the substituition is not shown
utils.nmap('Replace on every buffer', '<leader>s', ':bufdo %s//ge<left><left><left>')
utils.nmap('Replace current word', '<leader>ss', [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
utils.nmap('Format paragraph', '<leader>.', 'gqip')
utils.vmap('Insert in visual mode', 'i', 'I')
utils.vmap('Append in visual mode', 'a', 'A')
utils.vmap('Insert in visual mode', 'I', ':norm 0i') -- TODO improve
utils.vmap('Append in visual mode', 'A', ':norm $a') -- TODO improve
-- utils.vmap('Prepend line in visual mode', 'I', function()
--     local user_input = vim.fn.input("Prepend line with: ")
--     vim.cmd.normal(vim.api.nvim_replace_termcodes('0i'..user_input..'<esc>', true, true, true))
-- end)
-- utils.vmap('Append line in visual mode', 'A', function()
--     local user_input = vim.fn.input("Append line with: ")
--     vim.cmd.normal(vim.api.nvim_replace_termcodes('$a'..user_input..'<esc>', true, true, true))
-- end)

-- diagnostics
utils.nmap('Go to next diagnostic', '[d', vim.diagnostic.goto_prev)
utils.nmap('Go to previous diagnostic', ']d', vim.diagnostic.goto_prev)
utils.nmap('Add buffer diagnostics to the location list.', '<leader>q', vim.diagnostic.setloclist)
utils.nmap('Show diagnostics in a floating window.', '<leader>e', vim.diagnostic.open_float)
utils.nmap('Next quickfix item', ']q', ':cnext<cr>')
utils.nmap('Previous quickfix item', '[q', ':cprevious<cr>')

-- lua require('utils').nmap('Replace current word', '<leader>ss', [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
-- lua require('utils').nmap('Replace on every buffer', '<leader>s', ':bufdo %s//ge<left><left><left>')

for name, direction in pairs { ['left'] = 'h', ['down'] = 'j', ['up'] = 'k', ['right'] = 'l' } do
    utils.nmap('Select ' .. name .. ' window', '<c-' .. name .. '>', function()
        vim.cmd('wincmd ' .. direction)
    end)
end

--[[
Not plugin-related sets
--]]

local set = vim.opt
local utils = require 'utils'

set.number = true
set.relativenumber = true
set.ruler = true
set.tabstop = 4
set.shiftwidth = 4
set.softtabstop = 0
set.expandtab = true
set.showmatch = true
set.matchtime = 1
set.showcmd = true
set.wildmenu = true
set.wildignorecase = true
set.wildmode = 'longest,list,full'
set.hidden = true
set.cursorline = true
set.lazyredraw = true
set.updatetime = 300
set.scrolloff = 3
set.clipboard:append 'unnamedplus'
set.mouse = ''
set.title = true
set.isfname:append '@-@' -- TODO change this
set.encoding = 'utf-8'
set.fileformats = { 'unix', 'dos', 'mac' }
set.splitright = true
set.swapfile = false
set.backup = false
set.undofile = true -- undo from closed files
set.autoindent = true
set.smartindent = true
set.wrap = true
set.autoread = true
set.viminfo:prepend '%'
set.ignorecase = false
set.incsearch = true
set.hlsearch = true
set.listchars = {
    eol = '↴',
    trail = '⋅',
    tab = '→→',
    extends = '>',
    precedes = '<',
}
set.list = true
set.backspace = { 'indent', 'eol', 'start' }
set.spelllang = 'en_us'
set.spell = true
set.modeline = true
set.modelines = 5
vim.g.netrw_silent = 1 -- suppress message when editing remote
vim.cmd [[let g:vimsyn_embed = 'lPr']]
utils.set_textwidth(80)

function trim_trailing_whitespaces()
    local view = vim.fn.winsaveview()
    vim.cmd [[ silent %s/\s\+$//e ]]
    vim.fn.winrestview(view)
end

local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
    desc = 'Highlight on yank',
    callback = function()
        vim.highlight.on_yank()
    end,
    group = highlight_group,
    pattern = '*',
})

vim.api.nvim_create_autocmd('VimLeavePre', {
    desc = 'Close all buffers on :quit',
    pattern = '*',
    command = '%bdelete',
})

vim.api.nvim_create_autocmd('BufReadPost', {
    desc = 'Return to last edit position when opening files',
    pattern = '*',
    command = [[ if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif ]],
    -- TODO translate this to a lua function
})

local remove_whitespaces = vim.api.nvim_create_augroup('remove_whitespaces', { clear = true })
vim.api.nvim_create_autocmd('BufWritePre', {
    desc = 'Trim trailing whitespaces on save',
    pattern = '*',
    command = 'lua trim_trailing_whitespaces()',
    group = remove_whitespaces,
})

vim.api.nvim_create_autocmd({ 'BufNewFile', 'BufRead' }, {
    desc = 'associate .h extension with C language',
    pattern = { '*.h' },
    callback = function()
        vim.bo.filetype = 'c'
    end,
})

-- common typos
vim.cmd [[
    cnoreabbrev W! w!
    cnoreabbrev W1 w!
    cnoreabbrev w1 w!
    cnoreabbrev Q! q!
    cnoreabbrev Q1 q!
    cnoreabbrev q1 q!
    cnoreabbrev Qa! qa!
    cnoreabbrev Qall! qall!
    cnoreabbrev qw wq
    cnoreabbrev Qw wq
    cnoreabbrev Wa wa
    cnoreabbrev Wq wq
    cnoreabbrev wQ wq
    cnoreabbrev WQ wq
    cnoreabbrev Wqa wqa
    cnoreabbrev WQA wqa
    cnoreabbrev wq1 wq!
    cnoreabbrev Wq1 wq!
    cnoreabbrev wQ1 wq!
    cnoreabbrev WQ1 wq!
    cnoreabbrev ẅ w
    cnoreabbrev ẅa wa
    cnoreabbrev ẅq wq
    cnoreabbrev ẅqa wqa
    cnoreabbrev W w
    cnoreabbrev Q q
    cnoreabbrev Qa qa
    cnoreabbrev Qall qall
]]

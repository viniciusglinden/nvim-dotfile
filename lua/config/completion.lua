if vim.lsp.inlay_hint then
    vim.lsp.inlay_hint.enable(true)
end

local border = 'single'
vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(vim.lsp.handlers.hover, {
    border = border,
})
vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(vim.lsp.handlers.signature_help, {
    border = border,
})

vim.diagnostic.config {
    virtual_text = {
        source = true,
    },
    float = {
        source = true,
        border = border,
    },
}

if require('utils').features_nvim_version '0.7' then
    vim.api.nvim_create_autocmd('CursorHold', {
        desc = 'Print diagnostic on a popup when hovering over it',
        callback = function()
            vim.diagnostic.open_float(nil, {
                focusable = false,
                close_events = { 'BufLeave', 'CursorMoved', 'InsertEnter', 'FocusLost' },
                border = 'rounded',
                source = 'always',
                prefix = ' ',
                scope = 'cursor',
            })
        end,
    })
end

vim.opt.completeopt = { 'menuone', 'noselect', 'preview' }
vim.opt.shortmess:append 'c'

local signs = { Error = ' ', Warn = ' ', Hint = '󰠠 ', Info = ' ' }
for type, icon in pairs(signs) do
    local hl = 'DiagnosticSign' .. type
    vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = '' })
end

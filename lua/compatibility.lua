-- Compatibility with older nvim versions
-- https://neovim.io/doc/user/deprecated.html
---@diagnostic disable: deprecated
---@diagnostic disable: duplicate-set-field

if type(vim.tbl_add_reverse_lookup) == 'nil' then
    vim.tbl_add_reverse_lookup = function(tbl)
        local reversed = {}
        for k, v in pairs(tbl) do
            reversed[v] = k
        end
        return reversed
    end
end

if type(vim.tbl_islist) == 'nil' then
    vim.tbl_islist = vim.islist
end

if type(vim.fs.joinpath) == 'nil' then
    -- vim.fs.joinpath is available only from version 0.10.0
    vim.fs.joinpath = function(...)
        local sep = package.config:sub(1, 1) -- Get OS-specific path separator
        local parts = { ... }
        local path = table.concat(parts, sep)

        -- Normalize redundant separators
        path = path:gsub(sep .. '+', sep)

        -- Remove trailing separator unless it's the root "/"
        if #path > 1 then
            path = path:gsub(sep .. '$', '')
        end

        return path
    end
end

if type(vim.lsp.buf_get_clients) == 'nil' then
    vim.lsp.buf_get_clients = vim.lsp.get_clients
end

-- Removing the annoying message by declaring the function again
if type(vim.diagnostic.is_enabled) ~= 'nil' then
    vim.diagnostic.is_disabled = function()
        return not vim.diagnostic.is_enabled()
    end
end

if type(vim.islist) ~= 'nil' then
    vim.tbl_islist = vim.islist
end

if type(vim.lsp.util.jump_to_location) ~= 'nil' then
    vim.lsp.util.jump_to_location = function(location, offset_encoding, _)
        vim.lsp.util.show_document(location, offset_encoding, { focus = true })
    end
end

if vim.loader then
    vim.loader.enable()
end

function table.find(tbl, value)
    for i, v in ipairs(tbl) do
        if v == value then
            return i
        end
    end
    return nil
end

table.pack = table.pack or function(...)
    return { n = select('#', ...), ... }
end
table.unpack = table.unpack or unpack

-- rbgrouleff/bclose.vim
local utils = require 'utils'

local function warn(msg)
    vim.api.nvim_echo({ { msg, 'ErrorMsg' } }, true, {})
end

local function bclose(bang, buffer)
    bang = bang and '!' or ''
    local target_buffer
    if buffer == '' then
        target_buffer = vim.fn.bufnr '%'
    elseif tonumber(buffer) then
        target_buffer = vim.fn.bufnr(tonumber(buffer))
    else
        target_buffer = vim.fn.bufnr(buffer)
    end

    if target_buffer < 0 then
        warn('No matching buffer for ' .. buffer)
        return
    end

    if bang == '' and vim.fn.getbufvar(target_buffer, '&modified') ~= 0 then
        warn('No write since last change for buffer ' .. target_buffer .. ' (use :Bclose!)')
        return
    end

    -- Numbers of windows that view target buffer which we will delete
    local wnums = {}
    for w = 1, vim.fn.winnr '$' do
        if vim.fn.winbufnr(w) == target_buffer then
            table.insert(wnums, w)
        end
    end

    local wcurrent = vim.fn.winnr()
    for _, w in ipairs(wnums) do
        vim.cmd(w .. 'wincmd w')
        local prevbuf = vim.fn.bufnr '#'
        if prevbuf > 0 and vim.fn.buflisted(prevbuf) == 1 and prevbuf ~= w then
            vim.cmd 'buffer #'
        else
            vim.cmd 'bprevious'
        end

        if target_buffer == vim.fn.bufnr '%' then
            -- Listed buffers that are not the target
            local blisted = {}
            for b = 1, vim.fn.bufnr '$' do
                if vim.fn.buflisted(b) == 1 and b ~= target_buffer then
                    table.insert(blisted, b)
                end
            end

            -- Listed, not target, and not displayed
            local bhidden = {}
            for _, b in ipairs(blisted) do
                if vim.fn.bufwinnr(b) < 0 then
                    table.insert(bhidden, b)
                end
            end

            -- Take the first buffer, if any
            local bjump = bhidden[1] or blisted[1] or -1
            if bjump > 0 then
                vim.cmd('buffer ' .. bjump)
            else
                vim.cmd('enew' .. bang)
            end
        end
    end

    vim.cmd('bdelete' .. bang .. target_buffer)
    vim.cmd(wcurrent .. 'wincmd w')
end

vim.api.nvim_create_user_command('Bclose', function(opts)
    bclose(opts.bang, opts.args)
end, { bang = true, nargs = '?', complete = 'buffer' })

utils.nmap('Close buffer', '<c-q>', function()
    bclose(true, vim.api.nvim_get_current_buf())
end)

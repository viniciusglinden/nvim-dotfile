local M = {
    _switch = {},
}

---Append case
---@param key string Key to associate value
---@param value any Value
function M:case_append(key, value)
    self._switch[key] = value
end

setmetatable(M, {
    __index = function(self, key)
        return self._switch[key]
    end,
})

return M

local M = {
    _stack = {},
}

setmetatable(M, {
    __index = M._stack,
})

--- Append value to the storage
---@param queue any Queue name
---@param value any Value to append
function M:add_to_queue(queue, value)
    self._stack[queue] = self._stack[queue] or {}
    table.insert(self._stack[queue], 1, value)
end

--- Get and remove a value from the stack
---@param stack any Stack name
---@return any Value previously stored in the stack
function M:get_value(stack, pos)
    pos = pos or 1
    pos = #self._stack[stack] - pos + 1
    return table.remove(self._stack[stack], pos)
end

--- Consume one element from the stack by applying a function to it
---@param stack any Stack name
---@param func function Function to use to consume
---@return any # Function result
function M:consume_element(stack, func)
    return func(self:get_value(stack))
end

--- Apply a function to one element of the stack
---@param stack any Stack name
---@param func function Function to use to apply
---@param pos integer Element position
---@return any # Function result
function M:apply_func(stack, func, pos)
    pos = pos or 1
    pos = #self._stack[stack] - pos + 1
    return func(self._stack[stack][pos])
end

--- Consume all elements from the queue by applying a function to it
---@param stack any Stack name
---@param func function Function to use to consume
---@return table # Table of function results
function M:consume_all(stack, func)
    local values = {}
    for _, value in ipairs(self[stack] or {}) do
        local ret = func(value)
        if 'nil' ~= type(ret) then
            table.insert(values, 1, ret)
        end
    end
    return values
end

return M

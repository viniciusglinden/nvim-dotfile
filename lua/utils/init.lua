--[[
Lua helper functions

TODO
- bind HightlightWord()
--]]

local M = {}

function M.split(inputstr, sep, as_key)
    if sep == nil then
        sep = '%s'
    end
    local tab = {}
    for str in string.gmatch(inputstr, '([^' .. sep .. ']+)') do
        if as_key then
            tab[str] = as_key
        else
            table.insert(tab, str)
        end
    end
    return tab
end

function M.table_reverse(tab, indexing)
    local reversed = {}
    if indexing then
        for key, _ in pairs(tab) do
            table.insert(reversed, key)
        end
    else
        for key, value in pairs(tab) do
            reversed[value] = key
        end
    end
    return reversed
end

---Return a table as a string
---@param o table Table to print
---@return string
function M.table_dump(o)
    if type(o) == 'table' then
        local s = '{ '
        for k, v in pairs(o) do
            if type(k) ~= 'number' then
                k = '"' .. k .. '"'
            end
            s = s .. '[' .. k .. '] = ' .. M.table_dump(v) .. ','
        end
        return s .. '} '
    else
        return tostring(o)
    end
end

--- Adds a new mapping.
--- @param desc string Description.
--- @param mode string|string[] Mode short-name, see |nvim_set_keymap()|.
--- Can also be list of modes to create mapping on multiple modes.
--- @param keys string Keybinding to map `func` to.
--- @param action string|function Function or VimL command.
--- @param opt? vim.keymap.set.Opts Options to set
--- Silent is true by default.
---
--- TODO Warn when reusing the same keymap. Make this behavior enabled per default.
function M.map(desc, mode, keys, action, opt)
    assert(desc, 'Description is missing')
    opt = opt or {}
    local default = { desc = desc, silent = true }
    opt = vim.tbl_extend('keep', opt, default)
    vim.keymap.set(mode, keys, action, opt)
end

function M.map_disable(mode, keys)
    vim.keymap.set(mode, keys, '<nop>')
end

--[[
n: normal
v: visual and select
s: select
x: visual
o: operator-pending
c: command-line
t: terminal
--]]
for _, mode in ipairs { 'n', 'v', 'x', 's', 'o', 'i', 'l', 'c', 't' } do
    local map = mode .. 'map'
    M[map] = function(desc, keys, func, opt)
        M.map(desc, mode, keys, func, opt)
    end

    local disable_map = mode .. 'map_disable'
    M[disable_map] = function(keys)
        M.map_disable(mode, keys)
    end
end

--- Find a valid existing file among alternatives
--- (e.g. {'path', 'to', 'file'}, {'other', 'path', 'to', 'search'}
--- @param ... table Table of paths to search
--- @return string|nil
function M.file_find(...)
    local args = table.pack(...)
    for i = 1, args.n do
        local path = vim.fs.joinpath(unpack(args[i]))
        if vim.fn.filereadable(path) ~= nil then
            return path
        end
    end
    return nil
end

function M.find_files(directories)
    local files = {}
    for _, directory in pairs(directories) do
        for file in io.popen('find "' .. directory .. '" -type f'):lines() do
            table.insert(files, file)
        end
    end
    return files
end

---Find executable inside a given path
---@param name string Name of the executable
---@param additional_paths string[] Paths to search
---@param prioritize_path boolean? Whether the PATH should be the first result
---@return string Path to executable
function M.find_executable(name, additional_paths, prioritize_path)
    if prioritize_path then
        local path = vim.fn.exepath(name)
        if path ~= '' then
            return path
        end
    end

    for _, path in ipairs(additional_paths) do
        local full_path = vim.fs.joinpath(path, name)
        if vim.fn.executable(full_path) == 1 then
            return full_path
        end
    end
    -- Fall back to system PATH lookup
    return vim.fn.exepath(name)
end

function M.reload()
    -- FIXME
    local files = M.find_files(vim.api.nvim_list_runtime_paths())
    require('plenary.reload').reload_module(files)
end

function M.append_modeline()
    local modeline = string.format(
        ' vim: set ts=%d sw=%d tw=%d %s :',
        vim.o.tabstop,
        vim.o.shiftwidth,
        vim.o.textwidth,
        vim.o.expandtab and '' or 'no'
    )
    modeline = vim.fn.substitute(vim.o.commentstring, '%s', modeline, '')
    vim.api.nvim_buf_set_lines(0, 0, 0, false, { modeline })
end

function M.str2bool(i)
    if i == 0 then
        return false
    end
    return true
end

---Set textwidth with column highlight
---@param textwidth integer
function M.set_textwidth(textwidth)
    vim.opt.textwidth = textwidth
    if M.max_width_autocmd_id ~= nil then
        vim.api.nvim_del_autocmd(M.max_width_autocmd_id)
    end
    M.max_width_autocmd_id = vim.api.nvim_create_autocmd({ 'BufRead' }, {
        pattern = '*',
        callback = function()
            ---@diagnostic disable-next-line: undefined-field
            vim.fn.matchadd('ColorColumn', '\\%' .. textwidth + 1 .. 'v\\s*\\zs\\S', 100)
            vim.cmd [[highlight ColorColumn ctermbg=1 guibg=DarkRed]]
        end,
    })
end

---Test if current neovim is at least a particular version
---@param version string major.minor version
---@return boolean
function M.features_nvim_version(version)
    return vim.fn.has('nvim-' .. version) == 1
end

---Test if buffer number is a quickfix buffer
---@param bufnr integer Buffer number
---@return boolean
function M.is_quickfix(bufnr)
    return vim.bo[bufnr].buftype == 'quickfix'
end

---Get buffer index from current buffer offset
---@param offset integer Offset to current buffer
---@return integer
function M.get_buffer_idx_relative_to(offset)
    assert(offset, 'invalid offset')
    local current_buf = vim.api.nvim_get_current_buf()
    local buffers = vim.api.nvim_list_bufs()
    local current_index = nil
    for i, buf in ipairs(buffers) do
        if buf == current_buf then
            current_index = i
        end
    end
    return (current_index + offset) % #buffers
end

---Set current buffer to the index
---@param index integer Buffer index
function M.set_current_buffer_index(index)
    local buffers = vim.api.nvim_list_bufs()
    assert((0 < index) and (index <= #buffers), 'invalid index')
    local buffer = buffers[index]
    vim.api.nvim_set_current_buf(buffer)
end

return M

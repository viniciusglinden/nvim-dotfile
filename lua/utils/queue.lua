local M = {
    _queue = {},
}

setmetatable(M, {
    __index = M._queue,
})

--- Append value to the queue
---@param queue any Queue name
---@param value any Value to append
function M:add(queue, value)
    self._queue[queue] = self._queue[queue] or {}
    table.insert(self._queue[queue], value)
end

--- Get and remove a value from the queue
---@param queue any Queue name
---@return any Value previously stored in the queue
function M:get_value(queue, pos)
    pos = pos or 1
    return table.remove(self._queue[queue], pos)
end

--- Consume one element from the queue by applying a function to it
---@param queue any Queue name
---@param func function Function to use to consume
---@return any # Function result
function M:consume_element(queue, func)
    return func(self:get_from_queue(queue))
end

--- Apply a function to one element of the queue
---@param queue any Queue name
---@param func function Function to use to apply
---@param pos integer Element position
---@return any # Function result
function M:apply_func(queue, func, pos)
    pos = pos or 1
    return func(self._queue[queue][pos])
end

--- Consume all elements from the queue by applying a function to it
---@param queue any Queue name
---@param func function Function to use to consume
---@return table # Table of function results
function M:consume_all(queue, func)
    local values = {}
    for _, value in ipairs(self[queue] or {}) do
        local ret = func(value)
        if 'nil' ~= type(ret) then
            table.insert(values, ret)
        end
    end
    return values
end

return M

return {
    'vhyrro/luarocks.nvim',
    priority = 1000,
    config = true,
    opts = {
        rocks = {
            -- name of the lua rocks to install
        },
    },
}

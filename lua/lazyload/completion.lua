-- icons: https://www.nerdfonts.com/cheat-sheet
local kind_icons = {
    Class = ' ',
    Color = ' ',
    Constant = ' ',
    Constructor = ' ',
    Enum = ' ',
    EnumMember = ' ',
    Event = '',
    Field = '',
    File = ' ',
    Folder = ' ',
    Function = ' ',
    Interface = '',
    Keyword = '',
    Method = 'm',
    Module = '',
    Operator = '',
    Property = '',
    Reference = '',
    Snippet = '',
    Struct = ' ',
    Text = ' ',
    TypeParameter = '',
    Unit = '',
    Value = '',
    Variable = '',
}
local menu_icon = {
    nvim_lsp = 'λ',
    path = '',
    buffer = '﬘',
    luasnip = '',
    nvim_lua = '',
}

return {
    'hrsh7th/nvim-cmp',
    dependencies = {
        { 'hrsh7th/cmp-nvim-lsp', name = 'cmp-nvim-lsp' }, -- binding for LSP
        { 'hrsh7th/cmp-path' }, -- completion source: file system paths
        { 'hrsh7th/cmp-buffer' }, -- completion source: buffer words
        {
            -- snippet engine
            'L3MON4D3/LuaSnip',
            build = 'make install_jsregexp',
            dependencies = {
                -- completion source: luasnip
                'saadparwaiz1/cmp_luasnip',
            },
        },
        { 'hrsh7th/cmp-nvim-lsp-signature-help' }, -- parameter hints
        { 'hrsh7th/cmp-cmdline' }, -- completion source: command line
        { 'viniciusglinden/friendly-snippets' }, -- snippets
    },
    config = function()
        local cmp = require 'cmp' -- to use internal functions
        local luasnip = require 'luasnip'

        cmp.setup {
            sources = {
                -- order = priority
                { name = 'nvim_lsp' }, -- LSP
                { name = 'lazydev' }, -- nvim lua API
                {
                    -- snippets
                    name = 'luasnip',
                    option = {
                        use_show_condition = true,
                    },
                },
                { name = 'nvim_lsp_signature_help' }, -- function signature help
                { name = 'buffer' }, -- current buffer
                { name = 'path' }, -- OS path
            },
            mapping = {
                -- show completion options
                ['<c-c>'] = cmp.mapping.complete(),
                -- next suggestion
                ['<c-j>'] = cmp.mapping.select_next_item {
                    behavior = cmp.SelectBehavior.Insert,
                },
                -- previous suggested item
                ['<c-k>'] = cmp.mapping.select_prev_item {
                    behavior = cmp.SelectBehavior.Insert,
                },
                -- scroll documentation forwards
                ['<c-f>'] = cmp.mapping.scroll_docs(4),
                -- scroll documentation backwards
                ['<c-b>'] = cmp.mapping.scroll_docs(-4),
                -- abort
                ['<c-e>'] = cmp.mapping.abort(),
                -- confirm the currently selection suggestion
                ['<c-l>'] = cmp.mapping(
                    cmp.mapping.confirm {
                        behavior = cmp.SelectBehavior.Insert,
                        select = true,
                    },
                    { 'i', 'c' }
                ),
            },
            snippet = {
                expand = function(args)
                    luasnip.lsp_expand(args.body)
                end,
            },
            experimental = {
                ghost_text = true, -- shadow preview
            },
            window = {
                completion = cmp.config.window.bordered(),
                documentation = cmp.config.window.bordered(),
            },
            formatting = {
                fields = { 'abbr', 'kind' }, -- :help complete-items
                format = function(entry, item)
                    item.kind = kind_icons[item.kind]
                    item.menu = menu_icon[entry.source.name]
                    return item
                end,
            },
        }

        -- buffer source for `/` and `?` - does not work if `native_menu = true`
        cmp.setup.cmdline({ ':', '/', '?' }, {
            mapping = {
                ['<c-j>'] = {
                    c = function()
                        if cmp.visible() then
                            cmp.select_next_item()
                        else
                            cmp.complete()
                        end
                    end,
                },
                ['<c-k>'] = {
                    c = function()
                        if cmp.visible() then
                            cmp.select_prev_item()
                        else
                            cmp.complete()
                        end
                    end,
                },
                ['<c-e>'] = {
                    c = cmp.mapping.abort(),
                },
                ['<c-l>'] = {
                    c = cmp.mapping.confirm { select = false },
                },
            },
            sources = cmp.config.sources({
                { name = 'path' },
                { name = 'buffer' },
            }, {
                {
                    name = 'cmdline',
                    option = {
                        ignore_cmds = { 'Man', '!' },
                    },
                },
            }),
            matching = { disallow_symbol_nonprefix_matching = false },
        })

        luasnip.config.set_config {
            history = true,
            updateevents = 'TextChanged,TextChangedI',
        }

        local utils = require 'utils'
        utils.map('Expand or jump', { 'i', 's' }, '<c-l>', function()
            if luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            end
        end)

        utils.map('Jump backwards', { 'i', 's' }, '<c-h>', function()
            if luasnip.jumpable() then
                luasnip.jump(-1)
            end
        end)

        require('luasnip.loaders.from_vscode').lazy_load()
        -- Load custom snippets
        -- for _, path in ipairs(vim.api.nvim_get_runtime_file('snippets/*.lua', true)) do
        --     loadfile(path)()
        -- end
    end,
}

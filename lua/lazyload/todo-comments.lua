--[[
NOTE this requires ripgrep and fd
--]]

local keywords = { 'TODO', 'FIXME' }

return {
    'folke/todo-comments.nvim',
    dependencies = {
        { 'plenary' },
    },
    lazy = false,
    opts = {
        keywords = {
            FIXME = {
                icon = '🪛',
                -- color = info,
                alt = { 'FIX' },
            },
        },
        highlight = {
            pattern = [[.*<(KEYWORDS)]], -- TODO make this case insentive
        },
        search = {
            -- only used in keymaps
            command = 'rg',
            args = {
                '--color=never',
                '--no-heading',
                '--with-filename',
                '--line-number',
                '--column',
                '--ignore-case',
            },
            pattern = [[\b(KEYWORDS)]], -- ripgrep regex
        },
    },
    keys = {
        {
            desc = 'Next code commentary tag',
            ']t',
            function()
                require('todo-comments').jump_next { keywords = keywords }
            end,
        },
        {
            desc = 'Previous code commentary tag',
            '[t',
            function()
                require('todo-comments').jump_prev { keywords = keywords }
            end,
        },
    },
}

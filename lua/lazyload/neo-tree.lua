local copy_path = function(state)
    -- https://github.com/nvim-neo-tree/neo-tree.nvim/discussions/370
    local node = state.tree:get_node()
    local filepath = node:get_id()
    local filename = node.name
    local modify = vim.fn.fnamemodify

    local results = {
        filepath,
        modify(filepath, ':.'),
        modify(filepath, ':~'),
        filename,
        modify(filename, ':r'),
        modify(filename, ':e'),
    }

    local items = {
        'Absolute path: ' .. results[1],
        'Path relative to CWD: ' .. results[2],
        'Path relative to HOME: ' .. results[3],
        'Filename: ' .. results[4],
        'Filename without extension: ' .. results[5],
        'Extension of the filename: ' .. results[6],
    }
    vim.ui.select(items, {
        prompt = 'Choose to copy to clipboard:',
    }, function(choice)
        if choice then
            local i = table.find(items, choice)
            if i then
                local result = results[i]
                vim.fn.setreg('"', result)
                vim.fn.setreg('+', result)
                vim.notify('Copied: ' .. result)
            else
                vim.notify 'Invalid selection'
            end
        else
            vim.notify 'Selection cancelled'
        end
    end)
end

return {
    'nvim-neo-tree/neo-tree.nvim',
    dependencies = {
        { 'plenary' },
        { 'web-devicons' },
        { 'nui' },
        { 'window-picker' },
    },
    opts = {
        default_component_configs = {
            diagnostics = {
                symbols = { error = ' ', warn = ' ', hint = '󰠠 ', info = ' ' },
            },
        },
        source_selection = {
            winbar = false,
            statusline = false,
        },
        commands = {},
        sources = {
            'filesystem',
            'buffers',
            'git_status',
            'document_symbols',
        },
        window = {
            width = 33,
            mappings = {
                ['Y'] = { copy_path, desc = 'copy file path' },
                ['<space>'] = {
                    'toggle_node',
                    nowait = false, -- disable `nowait` if you have existing combos starting with this char that you want to use
                },
                ['<2-LeftMouse>'] = 'open',
                ['<cr>'] = 'open',
                ['<esc>'] = 'cancel',
                ['p'] = { 'toggle_preview', config = { use_float = true, use_image_nvim = true } },
                ['P'] = 'paste_from_clipboard',
                ['l'] = 'focus_preview',
                ['s'] = 'open_split',
                ['v'] = 'open_vsplit',
                ['S'] = 'split_with_window_picker',
                ['V'] = 'vsplit_with_window_picker',
                ['w'] = 'open_with_window_picker',
                ['C'] = 'close_node',
                -- ['C'] = 'close_all_subnodes',
                ['z'] = 'close_all_nodes',
                --['Z'] = 'expand_all_nodes',
                ['a'] = {
                    'add',
                    -- this command supports BASH style brace expansion ('x{a,b,c}' -> xa,xb,xc). see `:h neo-tree-file-actions` for details
                    -- some commands may take optional config options, see `:h neo-tree-mappings` for details
                    config = {
                        show_path = 'none', -- 'none', 'relative', 'absolute'
                    },
                },
                ['A'] = 'add_directory', -- also accepts the optional config.show_path option like 'add'. this also supports BASH style brace expansion.
                ['d'] = 'delete',
                ['r'] = 'rename',
                ['y'] = 'copy_to_clipboard',
                ['x'] = 'cut_to_clipboard',
                ['p'] = 'paste_from_clipboard',
                ['c'] = 'copy', -- takes text input for destination, also accepts the optional config.show_path option like 'add':
                ['m'] = 'move', -- takes text input for destination, also accepts the optional config.show_path option like 'add'.
                ['q'] = 'close_window',
                ['R'] = 'refresh',
                ['?'] = 'show_help',
                ['<'] = 'prev_source',
                ['>'] = 'next_source',
                ['i'] = 'show_file_details',
            },
        },
        filesystem = {
            filtered_items = {
                visible = false, -- hidden by default
                hide_dotfiles = true,
                hide_gitignored = false,
                hide_hidden = true,
            },
            follow_current_file = {
                enabled = false,
                leave_dirs_open = false,
            },
            group_empty_dirs = false,
            hijack_netrw_behavior = 'open_default',
            use_libuv_file_watcher = true, -- use the OS level file watchers to detect changes
            window = {
                mappings = {
                    ['<bs>'] = 'navigate_up',
                    ['H'] = 'set_root',
                    ['.'] = 'toggle_hidden',
                    ['/'] = 'fuzzy_finder',
                    ['D'] = 'fuzzy_finder_directory',
                    ['#'] = 'fuzzy_sorter', -- fuzzy sorting using the fzy algorithm
                    -- ['D'] = 'fuzzy_sorter_directory',
                    ['f'] = 'filter_on_submit',
                    ['<c-x>'] = 'clear_filter',
                    ['[g'] = 'prev_git_modified',
                    [']g'] = 'next_git_modified',
                    ['o'] = { 'show_help', nowait = false, config = { title = 'Order by', prefix_key = 'o' } },
                    ['oc'] = { 'order_by_created', nowait = false },
                    ['od'] = { 'order_by_diagnostics', nowait = false },
                    ['og'] = { 'order_by_git_status', nowait = false },
                    ['om'] = { 'order_by_modified', nowait = false },
                    ['on'] = { 'order_by_name', nowait = false },
                    ['os'] = { 'order_by_size', nowait = false },
                    ['ot'] = { 'order_by_type', nowait = false },
                    -- ['<key>'] = function(state) ... end,
                },
                fuzzy_finder_mappings = { -- define keymaps for filter popup window in fuzzy_finder_mode
                    ['<up>'] = 'move_cursor_up',
                    ['<down>'] = 'move_cursor_down',
                    ['<C-k>'] = 'move_cursor_up',
                    ['<C-j>'] = 'move_cursor_down',
                    -- ['<key>'] = function(state, scroll_padding) ... end,
                },
            },
            commands = {}, -- Add a custom command or override a global one using the same function name
        },
        buffers = {
            follow_current_file = {
                enabled = true,
                leave_dirs_open = false,
            },
            group_empty_dirs = true,
            show_unloaded = true,
            window = {
                mappings = {
                    ['bd'] = 'buffer_delete',
                    ['<bs>'] = 'navigate_up',
                    ['H'] = 'set_root',
                    ['o'] = { 'show_help', nowait = false, config = { title = 'Order by', prefix_key = 'o' } },
                    ['oc'] = { 'order_by_created', nowait = false },
                    ['od'] = { 'order_by_diagnostics', nowait = false },
                    ['om'] = { 'order_by_modified', nowait = false },
                    ['on'] = { 'order_by_name', nowait = false },
                    ['os'] = { 'order_by_size', nowait = false },
                    ['ot'] = { 'order_by_type', nowait = false },
                },
            },
        },
        git_status = {
            window = {
                position = 'float',
                mappings = {
                    ['A'] = 'git_add_all',
                    ['gu'] = 'git_unstage_file',
                    ['ga'] = 'git_add_file',
                    ['gr'] = 'git_revert_file',
                    ['gc'] = 'git_commit',
                    ['gp'] = 'git_push',
                    ['gg'] = 'git_commit_and_push',
                    ['o'] = { 'show_help', nowait = false, config = { title = 'Order by', prefix_key = 'o' } },
                    ['oc'] = { 'order_by_created', nowait = false },
                    ['od'] = { 'order_by_diagnostics', nowait = false },
                    ['om'] = { 'order_by_modified', nowait = false },
                    ['on'] = { 'order_by_name', nowait = false },
                    ['os'] = { 'order_by_size', nowait = false },
                    ['ot'] = { 'order_by_type', nowait = false },
                },
            },
        },
    },
    keys = {
        {
            desc = 'Toggle file navigator',
            '<c-f>',
            function()
                require('neo-tree.command').execute {
                    source = 'filesystem',
                    action = 'focus',
                    reveal = false,
                }
            end,
        },
        {
            desc = 'Toggle file navigator with focus on current file',
            '<cs-f>',
            function()
                require('neo-tree.command').execute {
                    source = 'filesystem',
                    action = 'focus',
                    reveal = true,
                }
            end,
        },
        {
            desc = 'Toggle symbols',
            '<c-s>',
            function()
                require('neo-tree.command').execute {
                    source = 'document_symbols',
                    action = 'focus',
                    reveal = true,
                }
            end,
        },
    },
}

--[[
:help lualine

TODO
add diagnostics
add buffer language

+-------------------------------------------------+
| A | B | C                             X | Y | Z |
+-------------------------------------------------+
--]]

return {
    'nvim-lualine/lualine.nvim',
    dependencies = {
        { 'web-devicons' },
    },
    opts = {
        options = {
            icons_enabled = true,
            theme = 'catppuccin',
            component_separators = { left = '', right = '' },
            section_separators = { left = '', right = '' },
            globalstatus = true, -- do not divide on windows
        },

        sections = {
            lualine_a = {
                'mode',
            },
            lualine_b = {
                'branch',
                'diff',
                {
                    'diagnostics',
                    symbols = { error = ' ', warn = ' ', hint = '󰠠 ', info = ' ' },
                },
            },
            lualine_c = {
                {
                    'filename',
                    file_status = true,
                    newfile_status = true,
                    path = 1,
                    symbols = {
                        readonly = '[R]',
                    },
                },
            },
            -- right side
            lualine_x = {
                'encoding',
                'fileformat',
                'filetype',
            },
            lualine_y = {
                'progress', -- % of file
            },
            lualine_z = {
                'location', -- line:column
            },
        },
    },
}

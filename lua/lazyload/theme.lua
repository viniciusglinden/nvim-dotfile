return {
    'catppuccin/nvim',
    name = 'catppuccin',
    priority = 1000,
    lazy = false,
    init = function()
        vim.cmd.colorscheme 'catppuccin'
    end,
    opts = {
        flavour = 'mocha', -- latte, frappe, macchiato, mocha
        term_colors = true,
        transparent_background = true,
        integration = {
            -- all integrations listed at https://github.com/catppuccin/nvim
            dap = {
                enabled = true,
                enable_ui = true,
            },
            dap_ui = true,
            cmp = true,
            treesitter = true,
            telescope = true,
            neotree = true,
            indent_blankline = {
                enabled = true,
                scope_color = 'mocha',
                colored_indent_levels = false,
            },
            native_lsp = {
                enabled = true,
                virtual_text = {
                    errors = { 'italic' },
                    hints = { 'italic' },
                    warnings = { 'italic' },
                    information = { 'italic' },
                    ok = { 'italic' },
                },
                underlines = {
                    errors = { 'underline' },
                    hints = { 'underline' },
                    warnings = { 'underline' },
                    information = { 'underline' },
                    ok = { 'underline' },
                },
                inlay_hints = {
                    background = true,
                },
            },
        },
        native_lsp = {
            enabled = true,
            virtual_text = {
                errors = { 'italic' },
                hints = { 'italic' },
                warnings = { 'italic' },
                information = { 'italic' },
            },
            underlines = {
                errors = { 'underline' },
                hints = { 'underline' },
                warnings = { 'underline' },
                information = { 'underline' },
            },
        },
        color_overrides = {
            mocha = {
                base = '#000000',
            },
        },
        highlight_overrides = {
            mocha = function(color)
                return {
                    TabLineSel = { bg = color.pink },
                    NvimTreeNormal = { bg = color.none },
                    CmpBorder = { fg = color.surface2 },
                    Pmenu = { bg = color.none },
                    NormalFloat = { bg = color.none },
                    TelescopeBorder = { link = 'FloatBorder' },
                    Comment = { fg = color.text },
                    ['@constant.builtin'] = { fg = color.yellow, style = {} },
                    ['@comment'] = { fg = color.black, style = { 'italic' } },
                }
            end,
        },
    },
}

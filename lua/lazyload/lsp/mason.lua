return {
    {
        'williamboman/mason.nvim',
        name = 'mason',
        dependencies = {
            'williamboman/mason-lspconfig.nvim',
            name = 'mason-lspconfig',
        },
        config = true,
    },
}

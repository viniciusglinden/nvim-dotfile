-- Open source/header
local switch_source_header_splitcmd = function(lspname, bufnr, edit_command)
    -- original from https://github.com/ayamir/nvimdots/blob/main/lua/modules/configs/completion/servers/clangd.lua
    local lspconfig = require 'lspconfig'
    bufnr = bufnr or vim.api.nvim_get_current_buf()
    bufnr = lspconfig.util.validate_bufnr(bufnr)
    local lsp_client = lspconfig.util.get_active_client_by_name(bufnr, lspname)
    local params = { uri = vim.uri_from_bufnr(bufnr) }
    if lsp_client then
        lsp_client.request('textDocument/switchSourceHeader', params, function(err, result)
            if err then
                error(tostring(err))
            end
            if not result then
                vim.notify('Corresponding file can’t be determined', vim.log.levels.ERROR, { title = 'LSP Error!' })
                return
            end
            vim.api.nvim_command(edit_command .. ' ' .. vim.uri_to_fname(result))
        end)
    else
        vim.notify(
            'Method textDocument/switchSourceHeader is not supported by any active server on this buffer',
            vim.log.levels.ERROR,
            { title = 'LSP Error!' }
        )
    end
end

-- Show symbol info
local symbol_info = function(lspname)
    local lspconfig = require 'lspconfig'
    -- original from lspconfig, function was local
    local bufnr = vim.api.nvim_get_current_buf()
    bufnr = lspconfig.util.validate_bufnr(bufnr)
    local lsp_client = lspconfig.util.get_active_client_by_name(bufnr, lspname)
    if not lsp_client or not lsp_client.supports_method 'textDocument/symbolInfo' then
        return vim.notify('LSP client not found', vim.log.levels.ERROR)
    end
    local params = vim.lsp.util.make_position_params()
    lsp_client.request('textDocument/symbolInfo', params, function(err, res)
        if err or #res == 0 then
            -- Clangd always returns an error, there is not reason to parse it
            return
        end
        local container = string.format('container: %s', res[1].containerName) ---@type string
        local name = string.format('name: %s', res[1].name) ---@type string
        vim.lsp.util.open_floating_preview({ name, container }, '', {
            height = 2,
            width = math.max(string.len(name), string.len(container)),
            focusable = false,
            focus = false,
            border = require('lspconfig.ui.windows').default_options.border or 'single',
            title = 'Symbol Info',
        })
    end, bufnr)
end

local actions = require 'utils.queue'

actions:add('clangd', {
    desc = 'Open source/header in current buffer',
    keys = '<F3>',
    func = function()
        switch_source_header_splitcmd('clangd', 0, 'edit')
    end,
})
actions:add('clangd', {
    desc = 'Open source/header in another vertical split',
    keys = 'gv',
    func = function()
        switch_source_header_splitcmd('clangd', 0, 'vsplit')
    end,
})
actions:add('clangd', {
    desc = 'Show symbol information',
    keys = 'gI',
    func = function()
        symbol_info 'clangd'
    end,
})

return {
    'neovim/nvim-lspconfig',
    name = 'nvim-lspconfig',
    event = { 'BufReadPre', 'BufNewFile' },
    dependencies = {
        'cmp-nvim-lsp',
        'telescope',
        'mason-lspconfig',
        -- TODO 'antosha417/nvim-lsp-file-operations'
    },
    config = function()
        local lspconfig = require 'lspconfig'
        local cmp_nvim_lsp = require 'cmp_nvim_lsp'
        local telescope_builtin = require 'telescope.builtin'
        local mason_lspconfig = require 'mason-lspconfig'

        local augroup = vim.api.nvim_create_augroup('LspFormatting', {})

        --- Actions to perform when client attaches
        ---@param client table Contains whole LSP configuration table
        ---@param bufnr number Buffer number
        local on_attach = function(client, bufnr)
            local map = function(desc, keys, func, mode)
                assert(desc, 'Description is missing')
                desc = 'LSP: ' .. desc
                mode = mode or 'n'
                local opt = { desc = desc, silent = true, buffer = bufnr }
                vim.keymap.set(mode, keys, func, opt)
            end

            -- NOTE <c-t> goes back
            map('Show references', 'gr', telescope_builtin.lsp_references)
            map('Show definitions', 'gd', telescope_builtin.lsp_definitions)
            map('Show implementations', 'gi', telescope_builtin.lsp_implementations)
            map('Show type definitions', 'gt', telescope_builtin.lsp_type_definitions)
            map('Code action', '<leader>ca', vim.lsp.buf.code_action, { 'n', 'v' })
            map('Rename', '<leader>rn', vim.lsp.buf.rename)
            --
            map('Show line diagnostics', '<leader>d', vim.diagnostic.open_float)
            map('Go to next diagnostics', ']d', vim.diagnostic.goto_next)
            map('Go to previous diagnostics', '[d', vim.diagnostic.goto_prev)

            map('Show documentation', 'K', vim.lsp.buf.hover)
            map('Restart LSP', '<leader>rs', '<cmd>LspRestart<cr>')

            actions:consume_all(client.config.name, function(action)
                map(action.desc, action.keys, action.func)
            end)

            if client.supports_method 'textDocument/formatting' then
                vim.api.nvim_clear_autocmds { group = augroup, buffer = bufnr }
                vim.api.nvim_create_autocmd('BufWritePre', {
                    desc = 'Auto format buffer',
                    group = augroup,
                    buffer = bufnr,
                    callback = function()
                        vim.lsp.buf.format { bufnr = bufnr }
                    end,
                })
            end
        end

        local default_config = {
            on_attach = on_attach,
            capabilities = vim.tbl_extend(
                'force',
                vim.lsp.protocol.make_client_capabilities(),
                cmp_nvim_lsp.default_capabilities()
            ),
            flags = {
                debounce_text_changes = 150, -- default in neovim 0.7+
            },
        }

        local configurator = {
            handlers = {
                -- apply default configuration
                function(server_name)
                    lspconfig[server_name].setup(default_config)
                end,
            },
        }

        ---Append new custom handler to the handlers list
        ---@param server_name string LSP server name according to lspconfig
        ---@param user_config table Setup according to lspconfig
        function configurator:handler_append(server_name, user_config)
            user_config = vim.tbl_extend('force', default_config, user_config)
            self.handlers[server_name] = function()
                lspconfig[server_name].setup(user_config)
            end
        end

        --[[
        For settings:
        :help lspconfig-all
        --]]

        configurator:handler_append('clangd', {
            -- https://clangd.llvm.org/config
            -- clangd --help
            filetypes = { 'c', 'cpp', 'objc', 'objcpp', 'cuda' },
            cmd = {
                'clangd',
                --[[
                -- .clangd files
                -- global clangd/config.yaml files
                -- to configure per project: https://clangd.llvm.org/config.html
                --]]
                '--enable-config',
                '--clang-tidy',
                '--background-index',
                '--fallback-style=Chromium',
                '--header-insertion=iwyu',
            },
            --[[ lets not have nvim commands here... lets use the keybindings
            commands = {
                ClangdShowSymbolInfo = {
                    function()
                        commands.symbol_info('clangd')
                    end,
                    description = 'Show symbol info',
                },
            }
            --]]
        })

        configurator:handler_append('rust_analyzer', {
            -- https://rust-analyzer.github.io/manual.html#configuration
            ['rust-analyzer'] = {
                diagnostics = {
                    enable = true,
                    disabled = {
                        'unlinked-file', -- single-file support
                    },
                },
                cargo = {
                    buildScripts = {
                        enable = true,
                    },
                },
            },
        })

        -- https://luals.github.io/wiki/settings/
        configurator:handler_append('lua_ls', {
            settings = {
                Lua = {
                    runtime = {
                        version = 'LuaJIT',
                    },
                    diagnostics = {
                        globals = { 'vim' },
                    },
                    workspace = {
                        library = vim.api.nvim_get_runtime_file('', true),
                        checkThirdParty = false,
                    },
                    codeLens = { enable = true },
                    telemetry = {
                        enable = false,
                    },
                },
            },
        })

        mason_lspconfig.setup_handlers(configurator.handlers)
    end,
}

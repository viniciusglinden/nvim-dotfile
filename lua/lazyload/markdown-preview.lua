return {
    'iamcco/markdown-preview.nvim',
    cmd = { 'MarkdownPreviewToggle', 'MarkdownPreview', 'MarkdownPreviewStop' },
    ft = { 'markdown' },
    build = function()
        vim.fn['mkdp#util#install']()
    end,
    init = function(...)
        vim.g.mkdp_browser = 'qutebrowser'
        vim.g.mkdp_echo_preview_url = 1
        vim.g.mkdp_auto_close = 0
        vim.g.mkdp_command_for_global = 1
        vim.g.mkdp_theme = 'dark'
    end,
    keys = {
        { desc = 'Launch markdown preview', '<F3>', ':MarkdownPreview<CR>', ft = 'markdown' },
    },
}

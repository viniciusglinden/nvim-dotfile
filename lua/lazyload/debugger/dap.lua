local open = function()
    vim.o.mouse = 'a'
    require('dapui').open()
end

local close = function()
    vim.o.mouse = ''
    require('dapui').close()
end

local icons = {
    Stopped = '🐞',
    Breakpoint = '🔴',
    BreakpointCondition = '🟡',
    LogPoint = '🟢',
    BreakpointRejected = '⚫',
}
for name, sign in pairs(icons) do
    vim.fn.sign_define('Dap' .. name, {
        text = sign,
        texthl = '',
        linehl = '',
        numhl = '',
    })
end

return {
    'mfussenegger/nvim-dap',
    dependencies = {
        {
            'rcarriga/nvim-dap-ui',
            opts = {
                expand_lines = require('utils').features_nvim_version '0.7',
            },
            dependencies = {
                { 'nvim-neotest/nvim-nio' },
            },
        },
        { 'jbyuki/one-small-step-for-vimkind' },
        {
            'Joakker/lua-json5',
            -- if you're on windows
            -- build = 'powershell ./install.ps1'
            build = './install.sh',
        },
    },
    lazy = false,
    config = function()
        local dap = require 'dap'
        local find_executable = require('utils').find_executable

        dap.defaults.fallback.external_terminal = {
            command = vim.fn.exepath 'ghostty',
            -- args = {},
        }
        dap.listeners.before.attach.dapui_config = open
        dap.listeners.before.launch.dapui_config = open
        dap.listeners.before.event_terminated.dapui_config = close
        dap.listeners.before.event_exited.dapui_config = close

        local cppdbg = find_executable('OpenDebugAD7', {
            vim.fs.joinpath(
                vim.fn.stdpath 'data',
                'mason',
                'packages',
                'cpptools',
                'extension',
                'debugAdapters',
                'bin'
            ),
        })
        if cppdbg ~= '' then
            if vim.fn.has 'win32' then
                dap.adapters.cppdbg = {
                    id = 'cppdbg',
                    type = 'executable',
                    command = cppdbg,
                    options = {
                        detached = false,
                    },
                }
            else
                dap.adapters.cppdbg = {
                    id = 'cppdbg',
                    type = 'executable',
                    command = cppdbg,
                }
            end
        end

        local debugpy = find_executable(
            'python',
            { vim.fs.joinpath(vim.fn.stdpath 'data', 'mason', 'packages', 'debugpy', 'venv', 'bin') }
        )
        if debugpy ~= '' then
            dap.adapters.debugpy = function(cb, config)
                if config.request == 'attach' then
                    local port = (config.connect or config).port
                    local host = (config.connect or config).host or '127.0.0.1'
                    cb {
                        type = 'server',
                        port = assert(port, '`connect.port` is required for a python `attach` configuration'),
                        host = host,
                        options = {
                            source_filetype = 'python',
                        },
                    }
                else
                    cb {
                        type = 'executable',
                        command = debugpy,
                        args = { '-m', 'debugpy.adapter' },
                        options = {
                            source_filetype = 'python',
                        },
                    }
                end
            end
        end

        local gdb = find_executable('gdb', { vim.fs.joinpath(vim.fn.stdpath 'data', 'mason', 'bin') })
        if gdb ~= '' then
            dap.adapters.gdb = {
                type = 'executable',
                command = gdb,
                args = { '-i', 'dap' },
            }
        end

        -- NOTE: REPL prepends a "return "
        local default_port = 8086
        dap.adapters.nlua = function(callback, config)
            callback {
                type = 'server',
                host = config.host or '127.0.0.1',
                port = config.port or default_port,
            }
        end

        ---Launch the NeoVim debugger server
        ---If port is below what the current user can get, server will be launched in a
        ---random port
        ---@param port? table|integer The port to attach to
        function launch_neovim_debugger_server(port)
            if 'table' == type(port) then
                port = tonumber(port.fargs[1])
            end
            require('osv').launch { port = port or default_port }
        end

        --[[
        1. Launch the server in the debuggee by calling this function
        2. Open another Neovim instance with the source file
        3. Place breakpoint
        4. Connect using the DAP client with the continue function
        --]]
        vim.api.nvim_create_user_command('LaunchNeovimDebuggerServer', launch_neovim_debugger_server, { nargs = '?' })
    end,
    keys = {
        {
            desc = 'DAP: Evaluate variable under the cursor',
            '<F4>',
            function()
                require('dapui').eval(nil, { enter = true })
            end,
        },
        {
            desc = 'DAP: Toggle breakpoint',
            '<F5>',
            function()
                require('dap').toggle_breakpoint()
            end,
        },
        {
            desc = 'DAP: Run to cursor',
            '<F6>',
            function()
                require('dap').run_to_cursor()
            end,
        },
        {
            desc = 'DAP: Step over',
            '<F8>',
            function()
                require('dap').step_over()
            end,
        },
        {
            desc = 'DAP: Step into',
            '<F9>',
            function()
                require('dap').step_into()
            end,
        },
        {
            desc = 'DAP: Step out',
            '<F10>',
            function()
                require('dap').step_out()
            end,
        },
        {
            desc = 'DAP: Step back',
            '<F11>',
            function()
                require('dap').step_back()
            end,
        },
        {
            desc = 'DAP: Restart',
            '<F12>',
            function()
                require('dap').restart()
            end,
        },
        {
            desc = 'DAP: Terminate',
            '<F12>',
            function()
                require('dap').terminate()
            end,
        },
        {
            desc = 'DAP: Run / continue',
            '<F7>',
            function()
                local launch = require('utils').file_find { '.vscode', 'launch.json' }
                if type(launch) == 'nil' then
                    print 'No debugging configuration file found'
                    return
                end

                require('dap.ext.vscode').json_decode = require('json5').parse -- C-style comments
                require('dap').continue()
            end,
        },
    },
}

return {
    'lukas-reineke/indent-blankline.nvim',
    dependencies = {
        { 'treesitter' },
    },
    main = 'ibl',
    config = true,
}

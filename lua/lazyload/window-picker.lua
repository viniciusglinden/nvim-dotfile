local go_to_window = function()
    local window_picker = require 'window-picker'
    ---@diagnostic disable-next-line: missing-parameter
    local picked_window_id = window_picker.pick_window()
    if nil ~= picked_window_id then
        vim.api.nvim_set_current_win(picked_window_id)
        return
    end

    local current_window = vim.api.nvim_get_current_win()
    local window_list = vim.api.nvim_tabpage_list_wins(0)
    for _, window_id in pairs(window_list) do
        if window_id ~= current_window then
            vim.api.nvim_set_current_win(window_id)
            return
        end
    end
end

return {
    's1n7ax/nvim-window-picker',
    name = 'window-picker',
    opts = {
        hint = 'floating-big-letter',
        selection_chars = 'FJDKSLA;CMRUEIWOQP',
    },
    keys = {
        { '<c-space>', go_to_window, desc = 'Switch to window directly with picker' },
    },
}

--[[
https://github.com/nvim-treesitter/nvim-treesitter#supported-languages
--]]

return {
    'nvim-treesitter/nvim-treesitter',
    name = 'treesitter',
    build = function()
        -- update parsers on sync
        local ts_update = require('nvim-treesitter.install').update { with_sync = true }
        ts_update()
    end,
    config = function()
        require('nvim-treesitter.configs').setup {
            ensure_installed = {
                'gitcommit',
                'gitignore',
            },
            sync_install = true,
            auto_install = true,
            highlight = {
                enable = true,

                disable = function(_, buf)
                    local max_filesize = 100 * 1024 -- 100 KB
                    local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
                    if ok and stats and stats.size > max_filesize then
                        vim.notify('Tree-sitter: File is too big, disabling syntax highlighting', vim.log.levels.WARN)
                        return true
                    end
                end,

                -- disable nvim builtin highlighting
                additional_vim_regex_highlighting = false,
            },
        }
    end,
}

return {
    'akinsho/bufferline.nvim',
    dependencies = {
        { 'catppuccin' },
        { 'web-devicons' },
    },
    init = function()
        vim.opt.termguicolors = true -- has to be true for this plugin
    end,
    config = function()
        require('bufferline').setup {
            highlights = require('catppuccin.groups.integrations.bufferline').get(),
            options = {
                custom_filter = function(buf_number)
                    -- ignore quickfix buffer
                    local buf_type = vim.bo[buf_number].buftype
                    return buf_type ~= 'quickfix'
                end,
                separator_style = 'slope',
                indicator = {
                    style = 'underline',
                },
            },
        }
    end,
}

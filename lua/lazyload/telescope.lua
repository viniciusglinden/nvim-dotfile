local append_current_promt_to_history = function(prompt_bufnr, picker)
    local action_state = require 'telescope.actions.state'
    local pkr = picker or action_state.get_current_picker(prompt_bufnr)
    local prompt_text = action_state.get_current_line()
    local history = action_state.get_current_history()
    history:append(prompt_text, pkr, false)
end

local edit_multiple_open = function(prompt_bufnr, picker)
    local action_state = require 'telescope.actions.state'
    local pkr = picker or action_state.get_current_picker(prompt_bufnr)
    if #(pkr:get_multi_selection()) > 1 then
        for _, entry in ipairs(pkr:get_multi_selection()) do
            vim.cmd(string.format(':e! %s', entry.value))
        end
    else
        require('telescope.actions').file_edit(prompt_bufnr)
    end
end

local multi_open = function(prompt_bufnr)
    append_current_promt_to_history(prompt_bufnr)
    edit_multiple_open(prompt_bufnr)
end

local use_window_picker = function(prompt_bufnr)
    local action_state = require 'telescope.actions.state'
    append_current_promt_to_history(prompt_bufnr)
    local picker = action_state.get_current_picker(prompt_bufnr)
    require('telescope.actions').close(prompt_bufnr)

    local window_picker_config = {
        filter_rules = {
            autoselect_one = false,
            include_current_win = true,
        },
    }
    local picked_window_id = require('window-picker').pick_window(window_picker_config)
    if picked_window_id then
        vim.api.nvim_set_current_win(picked_window_id)
    end
    -- edit files in current buffer if user cancels
    edit_multiple_open(prompt_bufnr, picker)
end

local peak_background = function(prompt_bufnr)
    local action_state = require 'telescope.actions.state'
    local builtin = require 'telescope.builtin'
    local prompt_text = action_state.get_current_line()
    local opts = {
        hidden = true,
        default_text = prompt_text,
    }
    require('telescope.actions').close(prompt_bufnr)
    print 'Press any key to continue Telescope prompt'
    vim.fn.getchar()
    builtin.resume(opts)
end

return {
    'nvim-telescope/telescope.nvim',
    name = 'telescope',
    dependencies = {
        { 'plenary' },
        {
            'nvim-telescope/telescope-fzf-native.nvim',
            build = 'make',
            cond = vim.fn.executable 'make' == 1,
        },
        { 'nvim-telescope/telescope-live-grep-args.nvim' },
        { 'nvim-telescope/telescope-dap.nvim' },
        { 'window-picker' },
    },
    config = function()
        local telescope = require 'telescope'
        local actions = require 'telescope.actions'
        local builtin = require 'telescope.builtin'
        local lga_actions = require 'telescope-live-grep-args.actions'
        local utils = require 'utils'
        -- enable telescope fzf native, if installed
        pcall(telescope.load_extension, 'fzf')
        telescope.load_extension 'live_grep_args'
        pcall(telescope.load_extension, 'dap')

        telescope.setup {
            extensions = {
                fzf = {
                    fuzzy = true,
                    override_generic_sorter = true,
                    override_file_sorter = true,
                    case_mode = 'smart_case',
                },
                live_grep_args = {
                    auto_quoting = true,
                    mappings = {
                        i = {
                            ['<C-q>'] = lga_actions.quote_prompt(), -- quote current
                            -- freeze the current list and start a fuzzy search in it
                            ['<C-space>'] = actions.to_fuzzy_refine,
                        },
                    },
                },
            },
            defaults = {
                layout_config = {
                    width = { padding = 0 },
                    height = { padding = 0 },
                    preview_width = 0.55,
                },
                file_ignore_patterns = {
                    'node_modules/',
                    '.git/',
                    '.nvm/',
                    '%.o',
                    '%.a$',
                    '%.out$',
                    '%.class$',
                    '%.pdf$',
                    '%.mkv$',
                    '%.mp4$',
                    '%.zip$',
                },
                mappings = {
                    i = {
                        ['<esc>'] = actions.close,
                        ['<c-c>'] = { '<esc>', type = 'command' },
                        ['<C-j>'] = actions.move_selection_next,
                        ['<C-k>'] = actions.move_selection_previous,
                        ['<C-p>'] = actions.cycle_history_prev,
                        ['<C-n>'] = actions.cycle_history_next,
                        ['<tab>'] = actions.toggle_selection + actions.move_selection_previous,
                        ['<s-tab>'] = actions.toggle_selection + actions.move_selection_next,
                        ['<cr>'] = multi_open,
                        ['<C-w>'] = use_window_picker,
                        ['<c-b>'] = peak_background,
                    },
                    n = {
                        ['<esc>'] = actions.close,
                        ['p'] = actions.cycle_history_prev,
                        ['n'] = actions.cycle_history_next,
                        ['<tab>'] = actions.toggle_selection + actions.move_selection_previous,
                        ['<s-tab>'] = actions.toggle_selection + actions.move_selection_next,
                        ['<cr>'] = fzf_multi_select,
                    },
                },
            },
            pickers = {
                find_files = {
                    hidden = true,
                },
            },
        }

        -- vimrc files as a table: vim.api.nvim_list_runtime_paths()
        utils.nmap('Find recently opened files', '<leader>f?', builtin.oldfiles)
        utils.nmap('Find existing buffers', '<leader>fb', builtin.buffers)
        utils.nmap('Search in current buffer', '<leader>f/', function()
            builtin.current_buffer_fuzzy_find(themes.get_dropdown {
                winblend = 0,
                previewer = false,
            })
        end)

        utils.nmap('Search Files', '<leader>ff', function()
            builtin.find_files { hidden = true, no_ignore = true }
        end)
        -- required ripgrep
        utils.nmap('Search by grep', '<leader>fg', telescope.extensions.live_grep_args.live_grep_args)
        utils.nmap('Search current Word', '<leader>fw', builtin.grep_string)
        utils.nmap('Search diagnostics', '<leader>fd', builtin.diagnostics)
        utils.nmap('Search git-tracked files only', '<leader>fG', builtin.git_files)
        utils.nmap('Continue last search', '<leader>fc', builtin.resume)
        utils.nmap('Search DAP command', '<leader>fv', telescope.extensions.dap.commands) -- FIX-ME

        utils.nmap('Search help', '<F1>', builtin.help_tags)
        utils.nmap('Search keybindings', '<F2>', builtin.keymaps) -- FIX-ME
    end,
}

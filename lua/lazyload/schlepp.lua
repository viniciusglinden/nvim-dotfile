-- TODO remove whitespaces after move

return {
    'zirrostig/vim-schlepp',
    keys = {
        { desc = 'Move text upwards', '<s-k>', '<Plug>SchleppIndentUp', mode = 'x' },
        { desc = 'Move text downwards', '<s-j>', '<Plug>SchleppIndentDown', mode = 'x' },
        { desc = 'Move text leftwards', '<s-h>', '<Plug>SchleppLeft', mode = 'x' },
        { desc = 'Move text rightwards', '<s-l>', '<Plug>SchleppRight', mode = 'x' },

        { desc = 'Move text upwards', '<up>', '<Plug>SchleppUp', mode = 'x' },
        { desc = 'Move text downwards', '<down>', '<Plug>SchleppDown', mode = 'x' },
        { desc = 'Move text leftwards', '<left>', '<Plug>SchleppLeft', mode = 'x' },
        { desc = 'Move text rightwards', '<right>', '<Plug>SchleppRight', mode = 'x' },
    },
}

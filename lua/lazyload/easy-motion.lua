package.path = package.path .. ';../?.lua'

return {
    'easymotion/vim-easymotion',

    init = function(...)
        vim.g.EasyMotion_do_mapping = 0 -- disable default mappings
        vim.g.EasyMotion_smartcase = 0
    end,

    keys = {
        { 'S', '<Plug>(easymotion-s)', desc = 'Easy motion jump' },
    },
}

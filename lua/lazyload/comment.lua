--[[
TODO
- open comment block above with Comment plugin
--]]

return {
    'numToStr/Comment.nvim',
    opts = {
        -- NORMAL: toggle
        toggler = {
            -- line-comment toggle keymap
            line = '<leader><leader>',
            -- block-comment toggle keymap
            block = 'gb',
        },
        -- NORMAL, VISUAL: operator pending
        opleader = {
            -- line-comment keymap
            line = '<leader><leader>',
            -- block-comment keymap
            block = 'gb',
        },
        extra = {
            above = 'gO',
            below = 'go',
        },
    },
}

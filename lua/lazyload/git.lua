return {
    {
        -- co — choose ours
        -- ct — choose theirs
        -- cb — choose both
        -- c0 — choose none
        -- ]x — move to previous conflict
        -- [x — move to next conflict
        'akinsho/git-conflict.nvim',
        config = true,
    },

    {
        'lewis6991/gitsigns.nvim',
        opts = {
            current_line_blame = true,
            current_line_blame_opts = {
                ignore_whitespace = true,
            },
            current_line_blame_formatter = ' <author> <abbrev_sha> <author_time:%Y-%m-%d> <summary>',
        },
    },
}

require('luasnip.session.snippet_collection').clear_snippets 'c'

local luasnip = require 'luasnip'

local snippet = luasnip.snippet
local insert = luasnip.insert_node
local format = require('luasnip.extras.fmt').fmt

luasnip.add_snippets('c', {
    snippet('fn', format('{} {}() {{\n{}', { insert(2), insert(1), insert(0) })),
})

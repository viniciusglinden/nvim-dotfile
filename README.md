
# Customization for specific setup

To change a default for a specific computer, add the overwrite somewhere in
`plugin/` folder. Example overwrite: `require 'utils'.set_textwidth(120)`.
